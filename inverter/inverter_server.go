package inverter

import (
	"log"
	"os"
	"os/signal"
	"pv-new-inverter-dpump/inverter/banpo_tcpLib"
	"pv-new-inverter-dpump/inverter/hanwoori_tcpLib"
	"pv-new-inverter-dpump/inverter/hyosung_modbusLib"
	"pv-new-inverter-dpump/inverter/naegok_tcpLib"
	"pv-new-inverter-dpump/inverter/scnoin_tcpLib"
	"pv-new-inverter-dpump/inverter/seocho_ssh"
	"pv-new-inverter-dpump/inverter/seocho_tcpLib"
	"pv-new-inverter-dpump/inverter/simsan_tcpLib"
	"time"

	"github.com/robfig/cron/v3"
)

/*
각 함수에 대한 설명의 경우 BanpoTcpClient를 참고하면 된다.
모두 같은 방식을 사용하고 있다.
해당 인버터 서버의 경우 서버 당 1개의 클라리언트만 접속이 가능하다.
따라서 connection refused가 되는 경우 다른 클라이언트가 접속해 있을 가능성이 높다.
*/
func RunServer() {

	go sendCommandScheduler()

	//반포
	go trySshConnect1()
	//심산
	go trySshConnect3()
	//한우리
	go trySshConnect4()
	//내곡, 서초종합
	go trySshConnect5()
	//서초노인
	go trySshConnect6()

	//효성 광덕
	go hyosung_modbusLib.RunHsInverter()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig
}

func wokrSsh1() {
	//ssh 10.8.1.1 login
	go seocho_ssh.SeochoSsh("10.8.1.22")
}

func wokrSsh3() {
	//ssh 10.8.1.3 login
	go seocho_ssh.SeochoSsh("10.8.1.3")
}

func wokrSsh4() {
	//ssh 10.8.1.4 login
	go seocho_ssh.SeochoSsh("10.8.1.4")
}

func wokrSsh5() {
	//ssh 10.8.1.5 login
	go seocho_ssh.SeochoSsh("10.8.1.5")
}

func wokrSsh6() {
	//ssh 10.8.1.6 login
	go seocho_ssh.SeochoSsh("10.8.1.6")
}

func workInverter() {
	//1번
	//반포
	go banpo_tcpLib.BanpoTcpClient()

	//내곡
	go naegok_tcpLib.NaegokTcpClient()

	//심산 1번
	go simsan_tcpLib.SimsanTcpClient(1)

	//한우리 1번
	go hanwoori_tcpLib.HanwooriTcpClient(1)

	//서초노인
	go scnoin_tcpLib.ScnoinTcpClient()

	//서초종합 1번
	go seocho_tcpLib.SeochoTcpClient(1)

	//2번
	time.Sleep(time.Second * 15)

	//심산 2번
	go simsan_tcpLib.SimsanTcpClient(2)

	//한우리 2번
	go hanwoori_tcpLib.HanwooriTcpClient(2)

	//서초종합 2번
	go seocho_tcpLib.SeochoTcpClient(2)

	//3번
	time.Sleep(time.Second * 15)

	//서초종합 3번
	go seocho_tcpLib.SeochoTcpClient(3)

	//4번
	time.Sleep(time.Second * 15)

	//서초종합 4번
	go seocho_tcpLib.SeochoTcpClient(4)

}

func sendCommandScheduler() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	//test 20초
	//_, err := c.AddFunc("0/20 * 6-19 * * *", workInverter)
	_, err := c.AddFunc("0 0/1 6-19 * * *", workInverter)
	if err != nil {
		log.Println(err)
	}
	c.Start()
}

func trySshConnect1() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	_, err := c.AddFunc("0 5,35 5-19 * * *", wokrSsh1)
	if err != nil {
		log.Println(err)
	}
	c.Start()
}

func trySshConnect3() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	_, err := c.AddFunc("0 10,40 5-19 * * *", wokrSsh3)
	if err != nil {
		log.Println(err)
	}
	c.Start()
}

func trySshConnect4() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	_, err := c.AddFunc("0 15,45 5-19 * * *", wokrSsh4)
	if err != nil {
		log.Println(err)
	}
	c.Start()
}

func trySshConnect5() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	_, err := c.AddFunc("0 20,50 5-19 * * *", wokrSsh5)
	if err != nil {
		log.Println(err)
	}
	c.Start()
}

func trySshConnect6() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	_, err := c.AddFunc("0 25,55 5-19 * * *", wokrSsh6)
	if err != nil {
		log.Println(err)
	}
	c.Start()
}
