package hyosung_modbusLib

import (
	"log"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/inverterDataLib"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/common/model"
	"reflect"
	"strconv"
	"time"

	"github.com/goburrow/modbus"
	"github.com/robfig/cron/v3"
)

/*
효성 인버터의 경우 tcp를 활용하는 방식이 아닌 모드버스를 활용하는 방식입니다.
golang의 경우 github 라이브러리를 바탕으로 쉽게 modbus tcp를 활용하여 데이터를 얻을 수 있습니다.
매분 00초를 주기로 모드버스 tcp 연결을 바탕으로 데이터를 얻어오고 비즈니스 로직에 맞춰 처리합니다.
해당 부분은 노션에 효성중공업 인버터 MODBUS맵을 검색하고 맨 위에 통신 프로토콜 문서를 바탕으로 데이터를 획득
*/

var hsMetaDataInfo model.HsMetaData

func hsModbusData() {
	// modbus tcp connect
	handler := modbus.NewTCPClientHandler(config.Env.HyosungModbus.Addr)
	handler.SlaveId = 1
	err := handler.Connect()
	if err != nil {
		log.Println(err)
	} else {
		hsClient := modbus.NewClient(handler)
		// 4byte 데이터 엔디안 변환 활용을 위한 변수
		createdDt, hsInverterInfo := inverterDataLib.HsProcessing(hsClient)
		hsMetaDataInfo = inverterDataLib.HsBinding(hsMetaDataInfo, config.Env.HyosungModbus.Addr, handler, createdDt, hsInverterInfo)

		log.Printf("Hs MetaDataInfo : %v", hsMetaDataInfo)

		//Write To influx
		tags := map[string]string{
			"uuid":    hsMetaDataInfo.UuId,
			"site_id": hsMetaDataInfo.SiteId,
			"sid":     hsMetaDataInfo.Serial,
			"id":      strconv.Itoa(hsMetaDataInfo.Measurement.Id),
		}

		target := reflect.ValueOf(&hsMetaDataInfo.Measurement)
		elements := target.Elem()
		fields := make(map[string]interface{})

		for i := 0; i < elements.NumField(); i++ {
			mValue := elements.Field(i)
			mType := elements.Type().Field(i)
			tag := mType.Tag
			if tag.Get("json") == "id" {
				continue
			} else {
				fields[tag.Get("json")] = mValue.Interface()
			}
		}

		createDate := hsMetaDataInfo.CreateDt
		layout := "2006-01-02T15:04:05Z"
		creatDtUTC, err := time.Parse(layout, createDate)
		if err != nil {
			log.Panic(err)
		}

		influx.InsertDataToInflux(err, "hyosung", tags, fields, creatDtUTC)

		//kafka sender
		if kafka.HsProduceToKafka(err, "hyosung", hsMetaDataInfo) {
			return
		}

		defer handler.Close()
	}
}

func RunHsInverter() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	_, err := c.AddFunc("0 0/1 * * * *", hsModbusData)
	if err != nil {
		log.Panic(err)
	}
	c.Start()
}
