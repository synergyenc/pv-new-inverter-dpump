package hanwoori_tcpLib

import (
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/inverterDataLib"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/common/model"
	"reflect"
	"strconv"
	"time"
)

var hanwooriClient net.Conn

func HanwooriTcpClient(slave int) {
	var err error
	for {
		log.Printf("Trying connection hanwooriClient %d", slave)
		hanwooriClient, err = net.DialTimeout("tcp", config.Env.HanwooriTcp.Addr, 5*time.Second)
		if err != nil {
			log.Printf("hanwooriClient dial connection err : %v", err)
			break
		} else if hanwooriClient == nil {
			log.Println("hanwooriClient is nil")
			break
		} else if hanwooriClient != nil {
			defer hanwooriClient.Close()
			log.Println("hanwooriClient is opened ")
		}

		go sendHanwooriCommand(slave)
		go HanwooriReceiveData()

		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, os.Kill)
		<-sig
	}
}

/**
Hanwoori1ReceiveData Processing flow
receive Inverter Data -> Processing -> binding ->
*/
func HanwooriReceiveData() {

	var hanwooriMetaDataInfo model.MetaData
	var hanwooriInverterDataInfoStruct model.InverterDataInfo

	data := make([]byte, 4096) // 4096 크기의 바이트 슬라이스 생성
	for {
		//log.Println("Waiting for the hanwoori data")
		hanwooriClient.SetReadDeadline(time.Now().Add(9 * time.Second))
		defer hanwooriClient.Close()
		n, err := hanwooriClient.Read(data) // 서버에서 받은 데이터를 읽음

		if err != nil {
			log.Println("hanwooriClient connect close")
			//hanwooriClient.Close()
			// if errClose := hanwooriClient.Close(); errClose != nil {
			// 	log.Println("error hanwooriClient connect close :", errClose)
			// }

			if err == io.EOF {
				break
			}
			//log.Println(err)
			break
		}

		if n > 0 {
			id := int(data[1])
			hanwooriInverterDataInfoStruct = inverterDataLib.Processing(id, data, n, err, hanwooriInverterDataInfoStruct)

			log.Printf("hanwoori inverterDatastruct : %v\n", hanwooriInverterDataInfoStruct)

			hanwooriMetaDataInfo = inverterDataLib.Binding(config.Env.HanwooriTcp.Addr, id, hanwooriMetaDataInfo, hanwooriInverterDataInfoStruct)

			//freq 주파수가 100 이상일 경우 튀는 데이터가 튀는 현상으로 판단되어 저장 안함
			if hanwooriMetaDataInfo.Measurement.GridFrequency <= 100 {
				//Write To influx
				tags := map[string]string{
					"uuid":    hanwooriMetaDataInfo.UuId,
					"site_id": hanwooriMetaDataInfo.SiteId,
					"sid":     hanwooriMetaDataInfo.Serial,
					"id":      strconv.Itoa(hanwooriMetaDataInfo.Measurement.Id),
				}

				target := reflect.ValueOf(&hanwooriMetaDataInfo.Measurement)
				elements := target.Elem()
				fields := make(map[string]interface{})

				for i := 0; i < elements.NumField(); i++ {
					mValue := elements.Field(i)
					mType := elements.Type().Field(i)
					tag := mType.Tag
					if tag.Get("json") == "id" {
						continue
					} else {
						fields[tag.Get("json")] = mValue.Interface()
					}
				}

				layout := "2006-01-02T15:04:05Z"
				creatDtUTC, err := time.Parse(layout, hanwooriMetaDataInfo.CreateDt)
				if err != nil {
					log.Panic(err)
				}

				influx.InsertDataToInflux(err, "hanwoori", tags, fields, creatDtUTC)

				//kafka sender
				if kafka.ProduceToKafka(err, "hanwoori", hanwooriMetaDataInfo) {
					log.Println(err)
				}
			}
		} else {
			log.Println("No Received Hanwoori Data")
		}
	}
}

func sendHanwooriCommand(slave int) {
	s1 := []byte{0x7e, 0x01, 0x07, 0x51, 0x8a}
	s2 := []byte{0x7e, 0x02, 0x07, 0x51, 0x7a}
	if slave == 1 {
		_, err := hanwooriClient.Write(s1) // 서버로 데이터를 보냄
		if err != nil {
			log.Println(err)
		}
	} else if slave == 2 {
		_, err := hanwooriClient.Write(s2)
		if err != nil {
			log.Println(err)
		}
	}
}
