package seocho_ssh

import (
	"fmt"
	"log"
	"net"
	"time"

	"golang.org/x/crypto/ssh"
)

// 패스워드 전달 방식과 타임아웃 전역변수 설정
const (
	CertPassword      = 1
	CertPublicKeyFile = 2
	DefaultTimeout    = 3 // Second
)

// SSH 접속에 필요한 정보를 담는 생성자
type SSH struct {
	IP      string
	User    string
	Cert    string //password or key file path
	Port    int
	session *ssh.Session
	client  *ssh.Client
}

// Connect the SSH Server
func (S *SSH) Connect(mode int) int {
	var sshConfig *ssh.ClientConfig
	var auth []ssh.AuthMethod
	if mode == CertPassword {
		auth = []ssh.AuthMethod{
			ssh.Password(S.Cert),
		}
	} else {
		log.Println("does not support mode: ", mode)
		return 0
	}

	sshConfig = &ssh.ClientConfig{
		User: S.User,
		Auth: auth,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},
		Timeout: time.Second * DefaultTimeout,
	}

	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", S.IP, S.Port), sshConfig)
	if err != nil {
		log.Println(err)
		return 0
	}

	session, err := client.NewSession()
	if err != nil {
		log.Println(err)
		client.Close()
		return 0
	}

	S.session = session
	S.client = client
	return 1
}

// RunCmd to SSH Server
func (S *SSH) RunCmd(cmd string) {
	out, err := S.session.CombinedOutput(cmd)

	if err != nil {
		log.Println(err)
	} else {
		//log.Println(string(out))
		const stdout = "this-is-stdout."
		const stderr = "this-is-stderr."
		g := string(out)
		if g != stdout+stderr && g != stderr+stdout {
			log.Println("Remote command did not return expected string:")
			log.Printf("want %q, or %q", stdout+stderr, stderr+stdout)
			log.Printf("got  %q", g)
		}
	}
}

func (S *SSH) Close() {
	S.session.Close()
	S.client.Close()
}

func SeochoSsh(ip string) {
	client := &SSH{
		IP:   ip,
		User: "admin",
		Port: 22,
		Cert: "sngy12#$",
	}
	log.Printf("SeochoSsh %s =================\n", ip)
	conInt := client.Connect(CertPassword)
	if conInt == 1 {
		client.RunCmd("/usr/sbin/iptables-restore < /jffs/etc/firewall.txt")
		client.Close()
		log.Printf("SeochoSsh %s close=================\n", ip)
	} else {
		log.Printf("SeochoSsh no connect %s close=================\n", ip)
	}
}
