package naegok_tcpLib

import (
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/inverterDataLib"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/common/model"
	"reflect"
	"strconv"
	"time"
)

var naegokClient net.Conn

func NaegokTcpClient() {
	var err error
	for {
		log.Println("Trying connection naegokClient 1")
		naegokClient, err = net.DialTimeout("tcp", config.Env.NaegokTcp.Addr, 5*time.Second)

		if err != nil {
			log.Printf("naegokClient dial connection err : %v", err)
			break
		} else if naegokClient == nil {
			log.Println("naegokClient is nil")
			break
		} else if naegokClient != nil {
			defer naegokClient.Close()
			log.Println("naegokClient is opened ")
		}

		go sendNaegokCommand()
		go NaegokReceiveData()

		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, os.Kill)
		<-sig
	}
}

func NaegokReceiveData() {

	var naegokMetaDataInfo model.MetaData
	var naegokInverterDataInfoStruct model.InverterDataInfo

	data := make([]byte, 4096) // 4096 크기의 바이트 슬라이스 생성
	for {
		//log.Println("Waiting for the naegok data")
		naegokClient.SetReadDeadline(time.Now().Add(9 * time.Second))
		defer naegokClient.Close()
		n, err := naegokClient.Read(data) // 서버에서 받은 데이터를 읽음

		if err != nil {
			log.Println("connect close naegokClient")
			//naegokClient.Close()
			// if errClose := naegokClient.Close(); errClose != nil {
			// 	log.Println("error naegokClient connect close :", errClose)
			// }

			if err == io.EOF {
				break
			}
			//log.Println(err)
			break
		}

		if n > 0 {
			id := int(data[1])
			naegokInverterDataInfoStruct = inverterDataLib.Processing(id, data, n, err, naegokInverterDataInfoStruct)

			log.Printf("naegok inverterDatastruct : %v\n", naegokInverterDataInfoStruct)

			naegokMetaDataInfo = inverterDataLib.Binding(config.Env.NaegokTcp.Addr, id, naegokMetaDataInfo, naegokInverterDataInfoStruct)

			//freq 주파수가 100 이상일 경우 튀는 데이터가 튀는 현상으로 판단되어 저장 안함
			if naegokMetaDataInfo.Measurement.GridFrequency <= 100 {
				//Write To influx
				tags := map[string]string{
					"uuid":    naegokMetaDataInfo.UuId,
					"site_id": naegokMetaDataInfo.SiteId,
					"sid":     naegokMetaDataInfo.Serial,
					"id":      strconv.Itoa(naegokMetaDataInfo.Measurement.Id),
				}

				target := reflect.ValueOf(&naegokMetaDataInfo.Measurement)
				elements := target.Elem()
				fields := make(map[string]interface{})

				for i := 0; i < elements.NumField(); i++ {
					mValue := elements.Field(i)
					mType := elements.Type().Field(i)
					tag := mType.Tag
					if tag.Get("json") == "id" {
						continue
					} else {
						fields[tag.Get("json")] = mValue.Interface()
					}
				}

				layout := "2006-01-02T15:04:05Z"
				creatDtUTC, err := time.Parse(layout, naegokMetaDataInfo.CreateDt)
				if err != nil {
					log.Panic(err)
				}

				influx.InsertDataToInflux(err, "naegok", tags, fields, creatDtUTC)

				//kafka sender
				if kafka.ProduceToKafka(err, "naegok", naegokMetaDataInfo) {
					log.Println(err)
				}
			}
		} else {
			log.Println("No Received Naegok Data")
		}
	}
}

func sendNaegokCommand() {
	s1 := []byte{0x7e, 0x01, 0x07, 0x51, 0x8a}
	_, err := naegokClient.Write(s1) // 서버로 데이터를 보냄

	if err != nil {
		log.Println(err)
	}

	//log.Println("Writing Naegok data to TCP Server is success")
}
