package banpo_tcpLib

import (
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/inverterDataLib"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/common/model"
	"reflect"
	"strconv"
	"time"
)

/*
각 클라이언트는 5초 간격으로 서버로 데이터를 보내주고 받은 데이터를 처리하도록 한다.
클라이언트와 서버 연결이 안될 경우 재연결이 가능하도록 코드를 구현(5초 뒤에 다시 접근 시도)
-> 밤 시간만 되면 계속해서 서버 쪽에서 연결을 거부함(이 부분은 추후에 어떻게 처리할 지 확인해봐야 함)
*/

var banpoClient net.Conn

func BanpoTcpClient() {
	var err error

	for {
		log.Println("Trying connection banpoClient 1")
		banpoClient, err = net.DialTimeout("tcp", config.Env.BanpoTcp.Addr, 5*time.Second)
		/*if banpoClient == nil || err != nil {
			log.Printf("banpo dial connection err : %v", err)
			//log.Println("Trying to reset the connection")
			//time.Sleep(5 * time.Second)
			break
		}*/

		if err != nil {
			log.Printf("banpoClient dial connection err : %v", err)
			break
		} else if banpoClient == nil {
			log.Println("banpoClient is nil")
			break
		} else if banpoClient != nil {
			defer banpoClient.Close()
			log.Println("banpoClient is opened ")
		}

		go sendBanpoCommand()
		go BanpoReceiveData()

		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, os.Kill)
		<-sig

	}
}

/*
해당 부분은 데이터를 받아서 로직을 처리하는 부분으로 위 BanpoTcpClient 함수에서 비동기로 돌린다. 비동기로 계속 기다렸다가
데이터가 쓰여지는 스케줄러가 동작하여 받은 데이터를 바탕으로 처리하도록 한다.
*/
func BanpoReceiveData() {

	var banpoMetaDataInfo model.MetaData
	var banpoInverterDataInfoStruct model.InverterDataInfo

	data := make([]byte, 4096) // 4096 크기의 바이트 슬라이스 생성
	for {
		//log.Println("Waiting for the banpo data")
		banpoClient.SetReadDeadline(time.Now().Add(9 * time.Second))
		defer banpoClient.Close()
		n, err := banpoClient.Read(data) // 서버에서 받은 데이터를 읽음

		if err != nil {
			log.Println("connect close banpoClient")
			//banpoClient.Close()
			// if errClose := banpoClient.Close(); errClose != nil {
			// 	log.Println("error banpoClient connect close :", errClose)
			// }
			if err == io.EOF {
				break
			}
			//log.Println(err)
			break
		}

		if n > 0 {
			//id는 받은 데이터의 첫 번째 인덱스 위치의 바이트를 통해 획득 갸능
			id := int(data[1])
			banpoInverterDataInfoStruct = inverterDataLib.Processing(id, data, n, err, banpoInverterDataInfoStruct)

			log.Printf("banpo inverterDatastruct : %v\n", banpoInverterDataInfoStruct)

			banpoMetaDataInfo = inverterDataLib.Binding(config.Env.BanpoTcp.Addr, id, banpoMetaDataInfo, banpoInverterDataInfoStruct)

			//freq 주파수가 100 이상일 경우 튀는 데이터가 튀는 현상으로 판단되어 저장 안함
			if banpoMetaDataInfo.Measurement.GridFrequency <= 100 {
				//Write To influx
				tags := map[string]string{
					"uuid":    banpoMetaDataInfo.UuId,
					"site_id": banpoMetaDataInfo.SiteId,
					"sid":     banpoMetaDataInfo.Serial,
					"id":      strconv.Itoa(banpoMetaDataInfo.Measurement.Id),
				}

				target := reflect.ValueOf(&banpoMetaDataInfo.Measurement)
				elements := target.Elem()
				fields := make(map[string]interface{})

				for i := 0; i < elements.NumField(); i++ {
					mValue := elements.Field(i)
					mType := elements.Type().Field(i)
					tag := mType.Tag
					if tag.Get("json") == "id" {
						continue
					} else {
						fields[tag.Get("json")] = mValue.Interface()
					}
				}

				layout := "2006-01-02T15:04:05Z"
				creatDtUTC, err := time.Parse(layout, banpoMetaDataInfo.CreateDt)
				if err != nil {
					log.Panic(err)
				}

				influx.InsertDataToInflux(err, "banpo", tags, fields, creatDtUTC)

				//kafka sender
				if kafka.ProduceToKafka(err, "banpo", banpoMetaDataInfo) {
					log.Println(err)
				}
			}
		} else {
			log.Println("No Received Banpo Data")
		}
	}
}

/*
다음의 경우는 인버터가 하나이지만 서초 종합, 한우리, 심산 공장의 경우 여러 개의 인버터가 붙어 있다.
각 인버터마다 국번이 다르기 때문에 요청하는 데이터가 다르다. 하지만 데이터를 한 번에 같은 시간에 보내게 된다면
데이터가 overwrite 되기 때문에 5초의 간격을 두고 차례대로 데이터를 처리할 수 있도록 구현한다.
*/
func sendBanpoCommand() {
	s1 := []byte{0x7e, 0x01, 0x07, 0x51, 0x8a}
	_, err := banpoClient.Write(s1) // 서버로 데이터를 보냄

	if err != nil {
		log.Println(err)
	}

	//log.Println("Writing banpo data to TCP Server is success")
}

/*
해당 부분은 스케줄러 시간을 조정하는 부분으로 위 sendBanpoCommand 함수를 매분 00초에 돌리도록 구현
*/
/*func sendCommandScheduler() {
	location, _ := time.LoadLocation("Asia/Seoul")
	c := cron.New(cron.WithSeconds(), cron.WithLocation(location))
	_, err := c.AddFunc("0 0/1 6-19 * * *", sendBanpoCommand)
	if err != nil {
		log.Println(err)
	}
	c.Start()
}*/
