package seocho_tcpLib

import (
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/inverterDataLib"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/common/model"
	"reflect"
	"strconv"
	"time"
)

var seochoClient net.Conn

func SeochoTcpClient(slave int) {
	var err error

	for {
		log.Printf("Trying connection seochoClient %d", slave)
		seochoClient, err = net.DialTimeout("tcp", config.Env.SeochoTcp.Addr, 5*time.Second)
		if err != nil {
			log.Printf("seochoClient dial connection err : %v", err)
			break
		} else if seochoClient == nil {
			log.Println("seochoClient is nil")
			break
		} else if seochoClient != nil {
			defer seochoClient.Close()
			log.Println("seochoClient is opened ")
		}

		go sendSeochoCommand(slave)
		go SeochoReceiveData()

		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, os.Kill)
		<-sig
	}
}

func SeochoReceiveData() {

	var seochoMetaDataInfo model.MetaData
	var seochoInverterDataInfoStruct model.InverterDataInfo

	data := make([]byte, 4096) // 4096 크기의 바이트 슬라이스 생성
	for {
		//log.Println("Waiting for the seocho data")
		seochoClient.SetReadDeadline(time.Now().Add(9 * time.Second))
		defer seochoClient.Close()
		n, err := seochoClient.Read(data) // 서버에서 받은 데이터를 읽음
		if err != nil {
			log.Println("seochoClient connect close : ", err)
			//defer seochoClient.Close()
			// defer func() {
			// 	if err1 := seochoClient.Close(); err1 != nil {
			// 		log.Println("error seochoClient connect close :", err1)
			// 	}
			// }()
			// if errClose := seochoClient.Close(); errClose != nil {
			// 	log.Println("error seochoClient connect close :", errClose)
			// }

			if err == io.EOF {
				break
			}
			//log.Println(err)
			break
		}

		if n > 0 {
			id := int(data[1])

			seochoInverterDataInfoStruct = inverterDataLib.Processing(id, data, n, err, seochoInverterDataInfoStruct)

			log.Printf("seocho inverterDatastruct : %v\n", seochoInverterDataInfoStruct)

			seochoMetaDataInfo = inverterDataLib.Binding(config.Env.SeochoTcp.Addr, id, seochoMetaDataInfo, seochoInverterDataInfoStruct)

			//freq 주파수가 100 이상일 경우 튀는 데이터가 튀는 현상으로 판단되어 저장 안함
			if seochoMetaDataInfo.Measurement.GridFrequency <= 100 {
				//Write To influx
				tags := map[string]string{
					"uuid":    seochoMetaDataInfo.UuId,
					"site_id": seochoMetaDataInfo.SiteId,
					"sid":     seochoMetaDataInfo.Serial,
					"id":      strconv.Itoa(seochoMetaDataInfo.Measurement.Id),
				}

				target := reflect.ValueOf(&seochoMetaDataInfo.Measurement)
				elements := target.Elem()
				fields := make(map[string]interface{})

				for i := 0; i < elements.NumField(); i++ {
					mValue := elements.Field(i)
					mType := elements.Type().Field(i)
					tag := mType.Tag
					if tag.Get("json") == "id" {
						continue
					} else {
						fields[tag.Get("json")] = mValue.Interface()
					}
				}

				createDate := seochoMetaDataInfo.CreateDt
				layout := "2006-01-02T15:04:05Z"
				creatDtUTC, err := time.Parse(layout, createDate)
				if err != nil {
					log.Panic(err)
				}

				influx.InsertDataToInflux(err, "seocho", tags, fields, creatDtUTC)

				//kafka sender
				if kafka.ProduceToKafka(err, "seocho", seochoMetaDataInfo) {
					log.Println(err)
				}
			}
		} else {
			log.Println("No Received Seocho Data")
		}
	}
}

func sendSeochoCommand(slave int) {
	s1 := []byte{0x7e, 0x01, 0x07, 0x51, 0x8a}
	s2 := []byte{0x7e, 0x02, 0x07, 0x51, 0x7a}
	s3 := []byte{0x7e, 0x03, 0x07, 0x50, 0xea}
	s4 := []byte{0x7e, 0x04, 0x07, 0x52, 0xda}
	if slave == 1 {
		_, err := seochoClient.Write(s1) // 서버로 데이터를 보냄
		if err != nil {
			log.Println(err)
		}
	} else if slave == 2 {
		_, err := seochoClient.Write(s2)
		if err != nil {
			log.Println(err)
		}
	} else if slave == 3 {
		_, err := seochoClient.Write(s3)
		if err != nil {
			log.Println(err)
		}
	} else if slave == 4 {
		_, err := seochoClient.Write(s4)
		if err != nil {
			log.Println(err)
		}
	}
}
