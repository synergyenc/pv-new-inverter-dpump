package scnoin_tcpLib

import (
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/inverterDataLib"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/common/model"
	"reflect"
	"strconv"
	"time"
)

var scnoinClient net.Conn

func ScnoinTcpClient() {
	var err error
	for {
		log.Println("Trying connection scnoinClient 1")
		scnoinClient, err = net.DialTimeout("tcp", config.Env.ScnoinTcp.Addr, 5*time.Second)

		if err != nil {
			log.Printf("scnoinClient dial connection err : %v", err)
			break
		} else if scnoinClient == nil {
			log.Println("scnoinClient is nil")
			break
		} else if scnoinClient != nil {
			defer scnoinClient.Close()
			log.Println("scnoinClient is opened ")
		}

		go sendScnoinCommand()
		go ScnoinReceiveData()

		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, os.Kill)
		<-sig

	}
}

/**
ScnoinReceiveData Processing flow
receive Inverter Data -> Processing -> binding ->
*/
func ScnoinReceiveData() {

	var scnoinMetaDataInfo model.MetaData
	var scnoinInverterDataInfoStruct model.InverterDataInfo

	data := make([]byte, 4096) // 4096 크기의 바이트 슬라이스 생성
	for {
		//log.Println("Waiting for the scnoin data")
		scnoinClient.SetReadDeadline(time.Now().Add(9 * time.Second))
		defer scnoinClient.Close()
		n, err := scnoinClient.Read(data) // 서버에서 받은 데이터를 읽음
		if err != nil {
			log.Println("connect close scnoinClient")
			//scnoinClient.Close()
			// if errClose := scnoinClient.Close(); errClose != nil {
			// 	log.Println("error scnoinClient connect close :", errClose)
			// }
			if err == io.EOF {
				break
			}
			//log.Println(err)
			break
		}

		if n > 0 {
			id := int(data[1])
			scnoinInverterDataInfoStruct = inverterDataLib.Processing(id, data, n, err, scnoinInverterDataInfoStruct)

			log.Printf("scnoin inverterDatastruct : %v\n", scnoinInverterDataInfoStruct)

			scnoinMetaDataInfo = inverterDataLib.Binding(config.Env.ScnoinTcp.Addr, id, scnoinMetaDataInfo, scnoinInverterDataInfoStruct)

			//freq 주파수가 100 이상일 경우 튀는 데이터가 튀는 현상으로 판단되어 저장 안함
			if scnoinMetaDataInfo.Measurement.GridFrequency <= 100 {
				//Write To influx
				tags := map[string]string{
					"uuid":    scnoinMetaDataInfo.UuId,
					"site_id": scnoinMetaDataInfo.SiteId,
					"sid":     scnoinMetaDataInfo.Serial,
					"id":      strconv.Itoa(scnoinMetaDataInfo.Measurement.Id),
				}

				target := reflect.ValueOf(&scnoinMetaDataInfo.Measurement)
				elements := target.Elem()
				fields := make(map[string]interface{})

				for i := 0; i < elements.NumField(); i++ {
					mValue := elements.Field(i)
					mType := elements.Type().Field(i)
					tag := mType.Tag
					if tag.Get("json") == "id" {
						continue
					} else {
						fields[tag.Get("json")] = mValue.Interface()
					}
				}

				layout := "2006-01-02T15:04:05Z"
				creatDtUTC, err := time.Parse(layout, scnoinMetaDataInfo.CreateDt)
				if err != nil {
					log.Panic(err)
				}

				influx.InsertDataToInflux(err, "scnoin", tags, fields, creatDtUTC)

				//kafka sender
				if kafka.ProduceToKafka(err, "scnoin", scnoinMetaDataInfo) {
					log.Println(err)
				}
			}
		} else {
			log.Println("No Received Scnoin Data")
		}
	}
}

func sendScnoinCommand() {
	s1 := []byte{0x7e, 0x01, 0x07, 0x51, 0x8a}
	_, err := scnoinClient.Write(s1) // 서버로 데이터를 보냄

	if err != nil {
		log.Println(err)
	}

	//log.Println("Writing Scnoin data to TCP Server is success")
}
