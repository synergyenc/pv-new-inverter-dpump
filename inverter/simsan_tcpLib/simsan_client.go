package simsan_tcpLib

import (
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/inverterDataLib"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/common/model"
	"reflect"
	"strconv"
	"time"
)

var simsanClient net.Conn

func SimsanTcpClient(slave int) {
	var err error

	for {
		/*simsanClient, err = net.Dial("tcp", config.Env.SimsanTcp.Addr)
		if simsanClient == nil || err != nil {
			log.Printf("simsan dial connection err : %v", err)
			time.Sleep(5 * time.Second)
		} else {
			defer simsanClient.Close() // main 함수가 끝나기 직전에 TCP 연결을 닫음

			go SimsanReceiveData()
			go sendCommandScheduler()

			sig := make(chan os.Signal)
			signal.Notify(sig, os.Interrupt, os.Kill)
			<-sig
		}*/
		log.Printf("Trying connection simsanClient %d", slave)
		simsanClient, err = net.DialTimeout("tcp", config.Env.SimsanTcp.Addr, 5*time.Second)
		if err != nil {
			log.Printf("simsanClient dial connection err : %v", err)
			break
		} else if simsanClient == nil {
			log.Println("simsanClient is nil")
			break
		} else if simsanClient != nil {
			defer simsanClient.Close()
			log.Println("simsanClient is opened ")
		}

		go sendSimsanCommand(slave)
		go SimsanReceiveData()

		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, os.Kill)
		<-sig
	}

}

func SimsanReceiveData() {

	var simsanMetaDataInfo model.MetaData
	var simsanInverterDataInfoStruct model.InverterDataInfo

	data := make([]byte, 4096) // 4096 크기의 바이트 슬라이스 생성
	for {
		//log.Println("Waiting for the simsan data")
		simsanClient.SetReadDeadline(time.Now().Add(9 * time.Second))
		defer simsanClient.Close()
		n, err := simsanClient.Read(data) // 서버에서 받은 데이터를 읽음
		if err != nil {
			log.Println("simsanClient connect close")
			//simsanClient.Close()
			// if errClose := simsanClient.Close(); errClose != nil {
			// 	log.Println("error simsanClient connect close :", errClose)
			// }
			if err == io.EOF {
				break
			}
			//log.Println(err)
			break
		}

		if n > 0 {
			id := int(data[1])
			simsanInverterDataInfoStruct = inverterDataLib.Processing(id, data, n, err, simsanInverterDataInfoStruct)

			log.Printf("simsan inverterDatastruct : %v\n", simsanInverterDataInfoStruct)

			simsanMetaDataInfo = inverterDataLib.Binding(config.Env.SimsanTcp.Addr, id, simsanMetaDataInfo, simsanInverterDataInfoStruct)

			//freq 주파수가 100 이상일 경우 튀는 데이터가 튀는 현상으로 판단되어 저장 안함
			if simsanMetaDataInfo.Measurement.GridFrequency <= 100 {
				//Write To influx
				tags := map[string]string{
					"uuid":    simsanMetaDataInfo.UuId,
					"site_id": simsanMetaDataInfo.SiteId,
					"sid":     simsanMetaDataInfo.Serial,
					"id":      strconv.Itoa(simsanMetaDataInfo.Measurement.Id),
				}

				target := reflect.ValueOf(&simsanMetaDataInfo.Measurement)
				elements := target.Elem()
				fields := make(map[string]interface{})

				for i := 0; i < elements.NumField(); i++ {
					mValue := elements.Field(i)
					mType := elements.Type().Field(i)
					tag := mType.Tag
					if tag.Get("json") == "id" {
						continue
					} else {
						fields[tag.Get("json")] = mValue.Interface()
					}
				}

				layout := "2006-01-02T15:04:05Z"
				creatDtUTC, err := time.Parse(layout, simsanMetaDataInfo.CreateDt)
				if err != nil {
					log.Panic(err)
				}

				influx.InsertDataToInflux(err, "simsan", tags, fields, creatDtUTC)

				//kafka sender
				if kafka.ProduceToKafka(err, "simsan", simsanMetaDataInfo) {
					log.Println(err)
				}
			}
		} else {
			log.Println("No Received Simsan Data")
		}
	}
}

func sendSimsanCommand(slave int) {
	s1 := []byte{0x7e, 0x01, 0x07, 0x51, 0x8a}
	s2 := []byte{0x7e, 0x02, 0x07, 0x51, 0x7a}
	if slave == 1 {
		_, err := simsanClient.Write(s1) // 서버로 데이터를 보냄
		if err != nil {
			log.Println(err)
		}
	} else if slave == 2 {
		_, err := simsanClient.Write(s2)
		if err != nil {
			log.Println(err)
		}
	}

	//log.Println("Writing Simsan1 data to TCP Server is success")

	//time.Sleep(time.Second * 5)

	//log.Println("Writing Simsan2 data to TCP Server is success")
}
