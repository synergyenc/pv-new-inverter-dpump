package main

import (
	"log"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/edms"
	"pv-new-inverter-dpump/common/influx"
	"pv-new-inverter-dpump/common/kafka"
	"pv-new-inverter-dpump/inverter"
	"time"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	time.Sleep(20 * time.Second) // vpn 연결 시간 고려

	err := config.GetEnvironmentVariable()
	if err != nil {
		log.Panic(err)
	}

	//kafka producer
	err = kafka.SetupProducer()
	if err != nil {
		log.Panic(err)
	}

	defer func() {
		err := kafka.Close()
		if err != nil {
			log.Panic(err)
		}
	}()

	//influx
	influx.SetupInflux()

	//edms request 호출
	edms.SetupEdmsClient()
	edms.RunGetDataFromEDMS()

	log.Println("inverter data collect server is started")

	// 해당 서버를 비동기적으로 처리하여 각 사이트 별로 만들어 놓은 함수들을 각각 비동기적으로 처리하도록 하였다.
	inverter.RunServer()

}
