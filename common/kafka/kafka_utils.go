package kafka

import (
	"encoding/json"
	"errors"
	"log"
	"pv-new-inverter-dpump/common/config"
	"pv-new-inverter-dpump/common/model"

	"github.com/Shopify/sarama"
)

/*
카프카 첫 세팅 및 프로듀서 관련 함수 구현
*/

var client sarama.SyncProducer
var topic string

func SetupProducer() error {
	cfg := sarama.NewConfig()
	switch config.Env.KafkaProducer.RequiredAcks {
	case "NoResponse":
		cfg.Producer.RequiredAcks = sarama.NoResponse
	case "WaitForLocal":
		cfg.Producer.RequiredAcks = sarama.WaitForLocal
	case "WaitForAll":
		cfg.Producer.RequiredAcks = sarama.WaitForAll
	default:
		log.Panicf("Unrecognized producer Ack option: %s", config.Env.KafkaProducer.RequiredAcks)
	}
	switch config.Env.KafkaProducer.Partitioner {
	case "NewRandomPartitioner":
		cfg.Producer.Partitioner = sarama.NewRandomPartitioner
	case "NewRoundRobinPartitioner":
		cfg.Producer.Partitioner = sarama.NewRoundRobinPartitioner
	default:
		log.Panicf("Unrecognized producer Partitioner: %s", config.Env.KafkaProducer.Partitioner)
	}
	cfg.Producer.Return.Successes = config.Env.KafkaProducer.Return.Successes
	var err error
	client, err = sarama.NewSyncProducer(config.Env.KafkaProducer.Server, cfg)
	if err != nil {
		return err
	}

	topic = config.Env.KafkaProducer.Topic
	return nil
}

func Close() error {
	err := client.Close()
	if err != nil {
		return err
	}
	return nil
}

func Produce(value string) (int32, int64, error) {
	if value == "" {
		return 0, 0, errors.New("msg is nil")
	}

	message := &sarama.ProducerMessage{}
	message.Topic = topic
	message.Value = sarama.StringEncoder(value)

	pid, offset, err := client.SendMessage(message)
	if err != nil {
		return 0, 0, err
	} else {
		return pid, offset, nil
	}
}

func ProduceToKafka(err error, name string, metaDataInfo model.MetaData) bool {
	kafkaDataMsg, err := json.Marshal(metaDataInfo)
	if err != nil {
		log.Println(err)
		return true
	}

	//pid, offset, err := Produce(string(kafkaDataMsg))
	pid, offset, err := Produce(string(kafkaDataMsg))
	if err != nil {
		log.Printf("kafka produce err : %v\n", err)
		return true
	} else if pid > 100 { //if pid > 100 실제 사용 시 해당 if문 제거, 바로 else
		log.Printf("%s -> pid:%v offset:%v\n", name, pid, offset)
	}

	return false
}

func HsProduceToKafka(err error, name string, metaDataInfo model.HsMetaData) bool {
	kafkaDataMsg, err := json.Marshal(metaDataInfo)
	if err != nil {
		log.Println(err)
		return true
	}

	pid, offset, err := Produce(string(kafkaDataMsg))

	if err != nil {
		log.Printf("kafka produce err : %v\n", err)
		return true
	} else {
		log.Printf("%s -> pid:%v offset:%v\n", name, pid, offset)
	}

	return false
}
