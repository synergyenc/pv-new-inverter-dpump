package model

/*
해당 부분은 인버터 모델들이 있는 부분입니다.
인버터 모델의 경우 현재 설치된 인버터와 효성 인버터가 데이터 형식이 조금 다르기 때문에 두 개의 데이터 struct를 만들었습니다.
*/

type MetaData struct{
	UuId 			string				`json:"uuid"`
	CreateDt		string				`json:"create_dt"`
	SiteId			string				`json:"site_id"`
	Serial      	string           	`json:"sid"`
	Measurement 	InverterDataInfo 	`json:"measurement"`
}

type HsMetaData struct{
	UuId 			string				`json:"uuid"`
	CreateDt		string				`json:"create_dt"`
	SiteId			string				`json:"site_id"`
	Serial			string				`json:"sid"`
	Measurement		HsInverterDataInfo	`json:"measurement"`
}

// InverterDataInfo : 모드버스 tcp 통신을 통해 얻은 실 데이터
type InverterDataInfo struct{
	Id								int			`json:"id"`
	PcsTotalAccumulativePowerAmount	float64		`json:"total_accu_pg"`
	InputVoltage					float64		`json:"in_v"`
	InputCurrent					float64		`json:"in_i"`
	InputPower						float64		`json:"in_p"`
	OutputRPhaseCurrent				float64		`json:"out_i_a"`
	OutputSPhaseCurrent				float64		`json:"out_i_b"`
	OutputTPhaseCurrent				float64		`json:"out_i_c"`
	OutputAverageCurrent			float64		`json:"out_avg_phase_i"`
	RSGridVoltage					float64		`json:"out_v_ab"`
	STGridVoltage					float64		`json:"out_v_bc"`
	RTGridVoltage					float64		`json:"out_v_ac"`
	OutputAverageVoltage			float64		`json:"out_avg_line_v"`
	InverterStatusInfo				int			`json:"status"`
	GridFrequency					float64		`json:"freq"`
	OutputActivePower				float64		`json:"sum_ap"`
	PowerFactor						float64		`json:"pf"`
}

type HsInverterDataInfo struct{
	Id								int			`json:"id"`
	PcsTotalAccumulativePowerAmount	float64		`json:"total_accu_pg"`
	InputVoltage					float64		`json:"in_v"`
	InputCurrent					float64		`json:"in_i"`
	InputPower						float64		`json:"in_p"`
	OutputRPhaseCurrent				float64		`json:"out_i_a"`
	OutputSPhaseCurrent				float64		`json:"out_i_b"`
	OutputTPhaseCurrent				float64		`json:"out_i_c"`
	OutputAverageCurrent			float64		`json:"out_avg_phase_i"`
	RSGridVoltage					float64		`json:"out_v_ab"`
	STGridVoltage					float64		`json:"out_v_bc"`
	RTGridVoltage					float64		`json:"out_v_ac"`
	OutputAverageVoltage			float64		`json:"out_avg_line_v"`
	InverterStatusInfo				int			`json:"status"`
	GridFrequency					float64		`json:"freq"`
	OutputActivePower				float64		`json:"sum_ap"`
	PcsTotalAccumulativePowerTime	int			`json:"total_accu_pg_time"`
	OutputReactivePower				float64		`json:"sum_rp"`
	PowerFactor						float64		`json:"pf"`
	PcsTemperature					float64		`json:"temp"`
}