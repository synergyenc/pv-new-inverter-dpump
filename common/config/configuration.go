package config

import (
	"errors"
	"log"
	"os"

	"github.com/influxdata/influxdb/client/v2"
	"github.com/spf13/viper"
)

/*
해당 부분은 config
*/
type Config struct {
	KafkaProducer producer
	Edms          edmsEnv
	Influx        Influx
	HyosungModbus hyosungModbus
	BanpoTcp      banPoTcp
	SimsanTcp     simsanTcp
	HanwooriTcp   hanwooriTcp
	ScnoinTcp     scnoinTcp
	SeochoTcp     seochoTcp
	NaegokTcp     naegokTcp
}

type producer struct {
	RequiredAcks string
	Partitioner  string
	Return       struct {
		Successes bool
	}
	Server []string
	Topic  string
}

type edmsEnv struct {
	CoreDNS    string
	ServiceKey string
}

type hyosungModbus struct {
	Addr string
}

type banPoTcp struct {
	Addr string
}

type simsanTcp struct {
	Addr string
}

type hanwooriTcp struct {
	Addr string
}

type scnoinTcp struct {
	Addr string
}

type seochoTcp struct {
	Addr string
}

type naegokTcp struct {
	Addr string
}

type Influx struct {
	Db          string
	Measurement string
	BaseUrl     string
}

var DB client.Client
var Env Config

func GetEnvironmentVariable() error {
	viper.SetConfigType("json")

	//배포
	viper.AddConfigPath("config")

	//로컬 테스트
	//viper.AddConfigPath("./common/config")

	if os.Getenv("env") == "prod" {
		viper.SetConfigName("prod.json")
	} else if os.Getenv("env") == "dev" {
		viper.SetConfigName("dev.json")
	} else {
		return errors.New("no config file")
	}

	var config Config

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err := viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	Env = config
	return nil
}
