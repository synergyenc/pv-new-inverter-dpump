package edms

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"pv-new-inverter-dpump/common/config"
)

/*
edms로부터 인버터의 접속 정보를 가져오기 위해 해당 코드 작성
한 번 호출하면 DeviceData에 인버터 관련 정보가 쌓인다.
따라서 데이터베이스에 데이터가 중간에 쌓이는 경우 pod을 재시작하여야 한다.(그럴 경우가 많지 않기 때문에 다름과 같이 구현하였다.)
먼 미래에는 저 부분도 스케줄러 또는 트리거를 활용해서 데이터베이스에 인버터 데이터가 추가될 경우 해당 데이터를 다시 한 번 불러올 수
있도록 하는 것이 좋을 것 같다.
*/
var DeviceData []map[string]interface{}

type edmsClient struct{
	serviceKey 		string
	coreDns			string
	httpClient 		*http.Client
}

var edmsCli edmsClient

func SetupEdmsClient() {
	edmsCli = edmsClient{
		serviceKey: config.Env.Edms.ServiceKey,
		coreDns:    config.Env.Edms.CoreDNS,
		httpClient: &http.Client{},
	}
}

func makeRequest(method string, url string, body io.Reader, query map[string]string)(*http.Request, error){
	req, err := http.NewRequest(method, edmsCli.coreDns + url, body)
	if err != nil{
		return nil, fmt.Errorf("[edms/request/makeRequest : %v", err)
	}

	req.Header.Add("X-Api-key", edmsCli.serviceKey)
	q := req.URL.Query()
	for key, value := range query {
		q.Add(key, value)
	}
	req.URL.RawQuery = q.Encode()
	return req, err
}

// GetDataFromEDMS : url 접속을 위한 함수
func GetDataFromEDMS() ([]map[string]interface{}, error){

	req, err := makeRequest("GET", "/v1/device/dpump?", nil, map[string]string{"type": "3"})
	resp, err := edmsCli.httpClient.Do(req)

	if resp != nil{
		defer resp.Body.Close()
	}

	if err != nil{
		return nil, err
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil{
		log.Fatal(err)
	}
	var result []map[string]interface{} // json 파일 저장
	err = json.Unmarshal(data, &result) // result에 unmarshal
	if err != nil{
		log.Fatal(err)
	}
	return result, nil
}

// RunGetDataFromEDMS : GetDataFromEDMS를 통해 전역변수로 설정한 deviceData에 값을 저장해줌.(하루에 한 번 스케줄링)
func RunGetDataFromEDMS(){
	var err error
	DeviceData, err = GetDataFromEDMS()
	if err != nil{
		log.Panic(err)
	}
}