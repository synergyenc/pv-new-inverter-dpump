package inverterDataLib

import (
	"encoding/binary"
	"fmt"
	"log"
	"pv-new-inverter-dpump/common/edms"
	"pv-new-inverter-dpump/common/model"
	"strconv"
	"time"

	"github.com/goburrow/modbus"
)

/*
해당 부분은 서버로부터 받은 데이터를 처리하는 부분
신재생 에너지 표준 프로토콜 가이드 문서를 참고해서 해당 데이터를 처리하였으며, 공통 부분이므로 data_utils로 빼서 사용
*/

func Processing(id int, data []byte, n int, err error, inverterDataInfoStruct model.InverterDataInfo) model.InverterDataInfo {

	result := data[5 : n-2]

	inputTotalVoltByte := result[:2]
	inputTotalCurByte := result[2:4]
	inputPowerByte := result[4:8]
	RSGridVoltByte := result[8:10]
	STGridVoltByte := result[10:12]
	RTGridVoltByte := result[12:14]
	outputRphaseCurByte := result[14:16]
	outputSphaseCurByte := result[16:18]
	outputTphaseCurByte := result[18:20]
	outputActivePowerByte := result[20:24]
	powerFactorByte := result[24:26]
	frequencyByte := result[26:28]
	totalAccumulativePowerByte := result[28:36]
	IsError := result[36:]

	//PcsTotalAccumulativePowerAmount, OutputReactivePower, PcsTemperature 정보 없음
	totalAccumulativePower := float64(binary.BigEndian.Uint64(totalAccumulativePowerByte))
	inputTotalVolt := float64(binary.BigEndian.Uint16(inputTotalVoltByte))
	inputTotalCur := float64(binary.BigEndian.Uint16(inputTotalCurByte))
	inputPower := float64(binary.BigEndian.Uint32(inputPowerByte)) * 0.001
	outputRphaseCur := float64(binary.BigEndian.Uint16(outputRphaseCurByte))
	outputSphaseCur := float64(binary.BigEndian.Uint16(outputSphaseCurByte))
	outputTphaseCur := float64(binary.BigEndian.Uint16(outputTphaseCurByte))
	outputAverageCurStr := fmt.Sprintf("%.2f", float64(outputRphaseCur+outputSphaseCur+outputTphaseCur)/3)
	outputAverageCur, err := strconv.ParseFloat(outputAverageCurStr, 64)
	if err != nil {
		log.Panic(err)
	}
	RSGridVolt := float64(binary.BigEndian.Uint16(RSGridVoltByte))
	STGridVolt := float64(binary.BigEndian.Uint16(STGridVoltByte))
	RTGridVolt := float64(binary.BigEndian.Uint16(RTGridVoltByte))
	outputAverageVoltStr := fmt.Sprintf("%.2f", float64(RSGridVolt+STGridVolt+RTGridVolt)/3)
	outputAverageVolt, err := strconv.ParseFloat(outputAverageVoltStr, 64)
	if err != nil {
		log.Panic(err)
	}
	inverterStatusInfoBinary := int(binary.BigEndian.Uint16(IsError))  // error code의 경우 16bit로 나타내서 각 bit 자리 수 별로 에러 코드를 확인해야 함.
	frequency := float64(binary.BigEndian.Uint16(frequencyByte)) * 0.1 // 10배의 값이 들어오므로
	outputActivePower := float64(binary.BigEndian.Uint32(outputActivePowerByte)) * 0.001
	powerFactor := float64(binary.BigEndian.Uint16(powerFactorByte)) * 0.1 // 10배의 값이 들어오므로

	inverterDataInfoStruct = model.InverterDataInfo{Id: id, PcsTotalAccumulativePowerAmount: totalAccumulativePower / 1000, InputVoltage: inputTotalVolt, InputCurrent: inputTotalCur, InputPower: inputPower,
		OutputRPhaseCurrent: outputRphaseCur, OutputSPhaseCurrent: outputSphaseCur, OutputTPhaseCurrent: outputTphaseCur, OutputAverageCurrent: outputAverageCur, RSGridVoltage: RSGridVolt, STGridVoltage: STGridVolt,
		RTGridVoltage: STGridVolt, OutputAverageVoltage: outputAverageVolt, InverterStatusInfo: inverterStatusInfoBinary, GridFrequency: frequency, OutputActivePower: outputActivePower, PowerFactor: powerFactor}

	return inverterDataInfoStruct
}

/*
binding의 경우 공장의 주소와 id를 바탕으로 기존에 edms를 호출하여 얻은 배열을 바탕으로 edms와 바인딩하여 데이터를 처리하는 부분이다.
*/
func Binding(addr string, id int, metaDataInfo model.MetaData, inverterDataInfoStruct model.InverterDataInfo) model.MetaData {
	for i := 0; i < len(edms.DeviceData); i++ {
		address, existAddr := edms.DeviceData[i]["info"].(map[string]interface{})["address"]
		edmsId, existId := edms.DeviceData[i]["info"].(map[string]interface{})["id"]
		createdDt := time.Now().UTC().Format("2006-01-02T15:04:05Z")
		if !(existAddr && existId) {
			continue
		}

		if addr == address.(string) && id == int(edmsId.(float64)) {
			metaDataInfo = model.MetaData{edms.DeviceData[i]["uuid"].(string), createdDt, edms.DeviceData[i]["site_id"].(string), edms.DeviceData[i]["sid"].(string),
				inverterDataInfoStruct}
		}
	}
	return metaDataInfo
}

func HsProcessing(hsClient modbus.Client) (string, model.HsInverterDataInfo) {

	var PcsTotalAccumulativePowerAmountByte []uint8
	var PcsTotalAccumulativePowerTimeByte []uint8

	//모드 버스 tcp 통한 바이트 값 확인
	//InverterIDByte, err := hsClient.ReadHoldingRegisters(2, 1)
	PcsTotalAccumulativePowerAmountByte1, err := hsClient.ReadHoldingRegisters(13, 1)
	PcsTotalAccumulativePowerAmountByte2, err := hsClient.ReadHoldingRegisters(14, 1)
	PcsTotalAccumulativePowerTimeByte1, err := hsClient.ReadHoldingRegisters(15, 1)
	PcsTotalAccumulativePowerTimeByte2, err := hsClient.ReadHoldingRegisters(16, 1)
	InputVoltageByte, err := hsClient.ReadHoldingRegisters(17, 1)
	InputCurrentByte, err := hsClient.ReadHoldingRegisters(18, 1)
	OutputRPhaseCurrentByte, err := hsClient.ReadHoldingRegisters(19, 1)
	OutputSPhaseCurrentByte, err := hsClient.ReadHoldingRegisters(20, 1)
	OutputTPhaseCurrentByte, err := hsClient.ReadHoldingRegisters(21, 1)
	RSGridVoltageByte, err := hsClient.ReadHoldingRegisters(22, 1)
	STGridVoltageByte, err := hsClient.ReadHoldingRegisters(23, 1)
	RTGridVoltageByte, err := hsClient.ReadHoldingRegisters(24, 1)
	GridFrequencyByte, err := hsClient.ReadHoldingRegisters(25, 1)
	OutputValidPowerByte, err := hsClient.ReadHoldingRegisters(26, 1)
	OutputInValidPowerByte, err := hsClient.ReadHoldingRegisters(27, 1)
	PowerFactorByte, err := hsClient.ReadHoldingRegisters(28, 1)
	PcsTemperatureByte, err := hsClient.ReadHoldingRegisters(29, 1)
	InverterStatusInfoByte, err := hsClient.ReadHoldingRegisters(62, 1)
	if err != nil {
		log.Panic(err)
	}

	// 모드버스의 경우 데이터를 읽을 때 2byte씩 읽지만 go modbus의 경우 1byte씩 데이터를 읽는다. 따라서 엔디안 변환 시 ABCD -> CDAB 이런 식으로 데이터를 읽어야 한다.
	PcsTotalAccumulativePowerAmountByte = []uint8{PcsTotalAccumulativePowerAmountByte2[0], PcsTotalAccumulativePowerAmountByte2[1], PcsTotalAccumulativePowerAmountByte1[0], PcsTotalAccumulativePowerAmountByte1[1]}
	PcsTotalAccumulativePowerTimeByte = []uint8{PcsTotalAccumulativePowerTimeByte2[0], PcsTotalAccumulativePowerTimeByte2[1], PcsTotalAccumulativePowerTimeByte1[0], PcsTotalAccumulativePowerTimeByte1[1]}

	// 빅엔디안 통한 결과값
	InverterID := 1                                                                                                //int(binary.BigEndian.Uint16(InverterIDByte))
	PcsTotalAccumulativePowerAmount := float64(binary.BigEndian.Uint32(PcsTotalAccumulativePowerAmountByte)) * 0.1 // pcs 총 누적발전량(kwh)
	PcsTotalAccumulativePowerTime := int(binary.BigEndian.Uint32(PcsTotalAccumulativePowerTimeByte))               // pcs 총 누적발전시간
	InputVoltage := float64(binary.BigEndian.Uint16(InputVoltageByte)) * 0.1                                       // 입력 전압 V
	InputCurrent := float64(binary.BigEndian.Uint16(InputCurrentByte)) * 0.1                                       // 입력 전류 A
	InputPower := InputVoltage * InputCurrent * 0.001                                                              //kW
	OutputRPhaseCurrent := float64(binary.BigEndian.Uint16(OutputRPhaseCurrentByte)) * 0.1                         // 출력 전류 R phase
	OutputSPhaseCurrent := float64(binary.BigEndian.Uint16(OutputSPhaseCurrentByte)) * 0.1                         // 출력 전류 S phase
	OutputTPhaseCurrent := float64(binary.BigEndian.Uint16(OutputTPhaseCurrentByte)) * 0.1                         // 출력 전류 T phase
	RSGridVoltage := float64(binary.BigEndian.Uint16(RSGridVoltageByte)) * 0.1                                     // 계통 전압 R-S Phase
	STGridVoltage := float64(binary.BigEndian.Uint16(STGridVoltageByte)) * 0.1                                     // 계통 전압 S-T Phase
	RTGridVoltage := float64(binary.BigEndian.Uint16(RTGridVoltageByte)) * 0.1                                     // 계통 전압 R-T Phase
	GridFrequency := float64(binary.BigEndian.Uint16(GridFrequencyByte)) * 0.1                                     // 계통 주파수
	OutputValidPower := float64(binary.BigEndian.Uint16(OutputValidPowerByte)) * 0.1                               // 출력 유효전력 kw
	OutputInValidPower := float64(binary.BigEndian.Uint16(OutputInValidPowerByte)) * 0.1                           // 출력 무효전력 kvar
	PowerFactor := float64(binary.BigEndian.Uint16(PowerFactorByte)) * 0.01                                        // 역률
	PcsTemperature := float64(binary.BigEndian.Uint16(PcsTemperatureByte))                                         // PCS 내부 온도
	InverterStatusInfo := binary.BigEndian.Uint16(InverterStatusInfoByte)                                          // 인버터 상태 정보
	// 추가 요구 정보
	OutputAverageCurrent := float64(OutputRPhaseCurrent+OutputSPhaseCurrent+OutputTPhaseCurrent) / 3 // 출력 전류
	OutputAverageVoltage := float64(RSGridVoltage+STGridVoltage+RTGridVoltage) / 3                   // 출력 전압

	//날짜의 경우 서버의 현재 시각 기준으로 가져옴(기존 모드버스에서 받아온 날짜 데이터의 경우 시간이 현재 시간과 다름
	createdDt := time.Now().UTC().Format("2006-01-02T15:04:05Z")

	// 광덕 pv data(json)
	var HsInverterInfo = model.HsInverterDataInfo{Id: InverterID, PcsTotalAccumulativePowerAmount: PcsTotalAccumulativePowerAmount, InputVoltage: InputVoltage, InputCurrent: InputCurrent, InputPower: InputPower, OutputRPhaseCurrent: OutputTPhaseCurrent, OutputSPhaseCurrent: OutputSPhaseCurrent,
		OutputTPhaseCurrent: OutputRPhaseCurrent, OutputAverageCurrent: OutputAverageCurrent, RSGridVoltage: RSGridVoltage, STGridVoltage: STGridVoltage, RTGridVoltage: RTGridVoltage, OutputAverageVoltage: OutputAverageVoltage, InverterStatusInfo: int(InverterStatusInfo), GridFrequency: GridFrequency,
		OutputActivePower: OutputValidPower, PcsTotalAccumulativePowerTime: PcsTotalAccumulativePowerTime, OutputReactivePower: OutputInValidPower, PowerFactor: PowerFactor, PcsTemperature: PcsTemperature}

	return createdDt, HsInverterInfo
}

func HsBinding(data model.HsMetaData, addr string, handler *modbus.TCPClientHandler, createdDt string, HsInverterInfo model.HsInverterDataInfo) model.HsMetaData {
	for i := 0; i < len(edms.DeviceData); i++ {
		if addr == edms.DeviceData[i]["info"].(map[string]interface{})["address"].(string) && int(handler.SlaveId) == int(edms.DeviceData[i]["info"].(map[string]interface{})["id"].(float64)) {
			data = model.HsMetaData{UuId: edms.DeviceData[i]["uuid"].(string), CreateDt: createdDt, SiteId: edms.DeviceData[i]["site_id"].(string), Serial: edms.DeviceData[i]["sid"].(string), Measurement: HsInverterInfo}
		}
	}
	return data
}
