package influx

import (
	"log"
	"pv-new-inverter-dpump/common/config"
	"time"

	"github.com/influxdata/influxdb/client/v2"
)

/*
인플럭스 데이터 베이스를 활용하기 위해 첫 세팅 및 데이터 베이스에 삽입하기 위한 함수 작성
*/

func SetupInflux() {
	var err error
	config.DB, err = client.NewHTTPClient(client.HTTPConfig{
		Addr: config.Env.Influx.BaseUrl,
	})
	if err != nil {
		log.Panic(err)
	}

	defer config.DB.Close()
}

func CreateBatchPoint() (client.BatchPoints, error) {
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  config.Env.Influx.Db,
		Precision: "ms",
	})
	if err != nil {
		return nil, err
	}

	return bp, nil
}

func CreatePoints(tags map[string]string, fields map[string]interface{}, handleTime time.Time) ([]*client.Point, error) {
	var pts []*client.Point
	pt, err := client.NewPoint(config.Env.Influx.Measurement, tags, fields, handleTime)
	if err != nil {
		return nil, err
	}
	pts = append(pts, pt)
	return pts, nil
}

func InsertDataToInflux(err error, name string, tags map[string]string, fields map[string]interface{}, creatDtUTC time.Time) {
	bp, err := CreateBatchPoint()
	if err != nil {
		log.Fatal(err)
	}
	pts, err := CreatePoints(tags, fields, creatDtUTC)
	if err != nil {
		log.Fatal(err)
	}
	bp.AddPoints(pts)
	err = config.DB.Write(bp)

	if err != nil {
		log.Fatal(err)
	} else {
		//log.Printf("%s -> tags : %v, fields : %v ", name, tags, fields)
	}
}
